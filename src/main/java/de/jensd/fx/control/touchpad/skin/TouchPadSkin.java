/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.touchpad.skin;

import com.sun.javafx.scene.control.skin.SkinBase;
import de.jensd.fx.control.touchpad.TouchPad;
import de.jensd.fx.control.touchpad.TouchPadBehavoir;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.CanvasBuilder;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CircleBuilder;
import javafx.util.Duration;

/**
 *
 * @author Jens Deters
 */
public class TouchPadSkin extends SkinBase<TouchPad, TouchPadBehavoir> {

  private static final double PREFERRED_SIZE = 200;
  private static final double MINIMUM_SIZE = 100;
  private static final double MAXIMUM_SIZE = 1000;
  private static final double PREF_TOUCH_POINT_SIZE = 10.0;
  private double size;
  private Canvas gridLayer;
  private Canvas labelLayer;
  private Canvas touchLayer;
  private Circle touchPoint;
  private DoubleProperty xPosProperty;
  private double defaultXPos = 0.0;
  private DoubleProperty yPosProperty;
  private double defaultYPos = 0.0;
  private Timeline moveBackLine;

  public TouchPadSkin(TouchPad control) {
    super(control, new TouchPadBehavoir(control));
    init();
    initComponents();
    registerProperties();
    attachListeners();
  }

  private void init() {
    xPosProperty = new SimpleDoubleProperty(this, "xPos", defaultXPos);
    yPosProperty = new SimpleDoubleProperty(this, "yPos", defaultYPos);

    if (Double.compare(getSkinnable().
            getPrefWidth(), 0.0) <= 0 || Double.compare(getSkinnable().
            getPrefHeight(), 0.0) <= 0
            || Double.compare(getSkinnable().
            getWidth(), 0.0) <= 0 || Double.compare(getSkinnable().
            getHeight(), 0.0) <= 0) {
      if (getSkinnable().
              getPrefWidth() > 0 && getSkinnable().
              getPrefHeight() > 0) {
        getSkinnable().
                setPrefSize(getSkinnable().
                getPrefWidth(), getSkinnable().
                getPrefHeight());
      } else {
        getSkinnable().
                setPrefSize(PREFERRED_SIZE, PREFERRED_SIZE);
      }
    }

    if (Double.compare(getSkinnable().
            getMinWidth(), 0.0) <= 0 || Double.compare(getSkinnable().
            getMinHeight(), 0.0) <= 0) {
      getSkinnable().
              setMinSize(MINIMUM_SIZE, MINIMUM_SIZE);
    }

    if (Double.compare(getSkinnable().
            getMaxWidth(), 0.0) <= 0 || Double.compare(getSkinnable().
            getMaxHeight(), 0.0) <= 0) {
      getSkinnable().
              setMaxSize(MAXIMUM_SIZE, MAXIMUM_SIZE);
    }
  }

  private void initComponents() {
    gridLayer = CanvasBuilder.create().
            build();
    labelLayer = CanvasBuilder.create().
            build();
    touchLayer = CanvasBuilder.create().
            build();

    touchPoint = CircleBuilder.create().
            radius(PREF_TOUCH_POINT_SIZE / 2.0).
            styleClass("touch-point").
            build();

    Pane root = new Pane();

    root.getChildren().
            addAll(gridLayer, labelLayer, touchPoint, touchLayer);

    getChildren().
            add(root);

  }

  private void registerProperties() {
    registerChangeListener(getSkinnable().
            widthProperty(), "resize");
    registerChangeListener(getSkinnable().
            heightProperty(), "resize");
    registerChangeListener(
            xPosProperty, "xPos");
    registerChangeListener(
            yPosProperty, "yPos");
    registerChangeListener(getSkinnable().
            moveBackOnReleaseProperty(), getSkinnable().
            moveBackOnReleaseProperty().
            getName());
    registerChangeListener(getSkinnable().
            animateMoveBackOnReleaseProperty(), getSkinnable().
            animateMoveBackOnReleaseProperty().
            getName());
    registerChangeListener(getSkinnable().
            valueModeProperty(), getSkinnable().
            valueModeProperty().
            getName());
    registerChangeListener(getSkinnable().
            xValueProperty(), getSkinnable().
            xValueProperty().
            getName());
    registerChangeListener(getSkinnable().
            yValueProperty(), getSkinnable().
            yValueProperty().
            getName());
  }

  private void attachListeners() {
    EventHandler<MouseEvent> touchEventHandler = new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent t) {
        handleTouchEvent(t);
      }
    };

    EventHandler<MouseEvent> touchReleasedEventHandler = new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent t) {
        handleTouchReleasedEvent(t);
      }
    };


    touchLayer.setOnMousePressed(touchEventHandler);
    touchLayer.setOnMouseDragged(touchEventHandler);
    touchLayer.setOnMouseReleased(touchReleasedEventHandler);

  }

  private void handleTouchEvent(MouseEvent t) {
    stopAnimation();


    double x = t.getX();
    double y = t.getY();

    // keep in jail
    if (Double.compare(x, 0.0) <= 0) {
      x = 0.0;
    } else if (x > size) {
      x = size;
    }

    if (Double.compare(y, 0.0) <= 0) {
      y = 0.0;
    } else if (y > size) {
      y = size;
    }
    drawCrosslines(x, y);
    xPosProperty.set(x);
    yPosProperty.set(y);
  }

  private void handleTouchReleasedEvent(MouseEvent t) {
    clearTouchLayer();
    if (getSkinnable().
            isMoveBackOnRelease()) {
      if (getSkinnable().
              isAnimateMoveBackOnRelease()) {
        moveBackLine = new Timeline(new KeyFrame(Duration.
                millis(getSkinnable().
                getAnimationDurationMillis()), new KeyValue(getSkinnable().
                xValueProperty(), 0.0, Interpolator.SPLINE(0.7, 0.2, 0.5, 1.0))),
                new KeyFrame(Duration.millis(getSkinnable().
                getAnimationDurationMillis()), new KeyValue(getSkinnable().
                yValueProperty(), 0.0, Interpolator.SPLINE(0.7, 0.2, 0.5, 1.0))));
        moveBackLine.setOnFinished(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent t) {
            getSkinnable().
                    setXValue(0.0);
            getSkinnable().
                    setYValue(0.0);
          }
        });

        moveBackLine.play();


      } else {
        getSkinnable().
                setXValue(0.0);
        getSkinnable().
                setYValue(0.0);
      }
    }
  }

  @Override
  protected void handleControlPropertyChanged(final String PROPERTY) {

    if ("resize".equals(PROPERTY)) {
      resize();
    } else if ("xPos".equals(PROPERTY) || "yPos".
            equals(PROPERTY)) {
      calculateValues();
    } else if (TouchPad.X_VALUE.equals(PROPERTY) || TouchPad.Y_VALUE.
            equals(PROPERTY)) {
      adjustTouchPointPosition(getSkinnable().
              getXValue(), getSkinnable().
              getYValue());
    } else if (TouchPad.VALUE_MODE.equals(PROPERTY)) {
      stopAnimation();
      getSkinnable().
              setXValue(0.0);
      getSkinnable().
              setYValue(0.0);
      adjustTouchPointPosition(getSkinnable().
              getXValue(), getSkinnable().
              getYValue());

      drawGrid();
      drawLabels();
    }


  }

  private void resize() {
    stopAnimation();
    size = getSkinnable().
            getWidth() < getSkinnable().
            getHeight() ? getSkinnable().
            getWidth() : getSkinnable().
            getHeight();

    gridLayer.setHeight(size);
    gridLayer.setWidth(size);

    labelLayer.setHeight(size);
    labelLayer.setWidth(size);

    touchLayer.setHeight(size);
    touchLayer.setWidth(size);

    adjustTouchPointPosition(getSkinnable().
            getXValue(), getSkinnable().
            getYValue());

    drawGrid();
    drawLabels();
  }

  private void stopAnimation() {
    if (moveBackLine != null) {
      moveBackLine.stop();
    }
  }

  /*
   * -------------- CALC STUFF --------------
   */
  private void calculateValues() {
    TouchPad.ValueMode mode = getSkinnable().
            getValueMode();

    if (mode != null) {
      double xValue;
      double yValue;
      switch (mode) {
        case ZERO_TO_ONE: // 0.0...1.0
          yValue = Math.abs(1 / size * (yPosProperty.
                  get() - size));
          xValue = 1 / size
                  * xPosProperty.
                  get();
          break;
        case NEGATIVE_TO_POSITIVE:
          // -1.0...+1.0
          xValue = ((1 / (size / 2)
                  * xPosProperty.
                  get()) - 1.0);
          yValue = -1 * ((1 / (size / 2)
                  * yPosProperty.
                  get()) - 1.0);
          break;
        default:
          xValue = Math.abs(1 / size * (yPosProperty.
                  get() - size));
          yValue = 1 / size
                  * xPosProperty.
                  get();
      }
      getSkinnable().
              setXValue(xValue);
      getSkinnable().
              setYValue(yValue);
    }
  }

  public void adjustTouchPointPosition(double xValue, double yValue) {
    if (getSkinnable().
            getValueMode() != null) {
      switch (getSkinnable().
              getValueMode()) {
        case ZERO_TO_ONE:
          // 0.0...1.0
          touchPoint.setCenterX(size * xValue);
          touchPoint.setCenterY(Math.abs(size * yValue - size));
          break;
        case NEGATIVE_TO_POSITIVE:
          // -1.0...+1.0
          touchPoint.setCenterX((size / 2) * (xValue + 1));
          touchPoint.setCenterY(Math.abs((size / 2) * (yValue + 1) - size));
          break;
      }
    }
  }

  /*
   * -------------- DRAW STUFF --------------
   */
  private void drawGrid() {
    GraphicsContext gc = gridLayer.getGraphicsContext2D();
    gc.clearRect(0, 0, size, size);

    //grid
    double step = size / 10;
    gc.setStroke(getSkinnable().
            getDataGridColor().
            darker());
    gc.setLineWidth(0.5);
    for (int i = 0; i < 10; i++) {
      gc.strokeLine(step * i, 0, step * i, size);
      gc.strokeLine(0, step * i, size, step * i);
    }

    gc.setStroke(getSkinnable().
            getDataGridColor().
            darker());
    gc.setLineWidth(2);

    // center axis
    if (TouchPad.ValueMode.NEGATIVE_TO_POSITIVE.equals(getSkinnable().
            getValueMode())) {
      gc.strokeRect(0, 0, size / 2.0, size / 2.0);
      gc.strokeRect(size / 2.0, size / 2.0, size, size);
    }

    //border
    gc.strokeRect(1.0, 1.0, size - 2.0, size - 2.0);
  }

  private void drawLabels() {
    GraphicsContext gc = labelLayer.getGraphicsContext2D();
    clearLabelLayer();

    TouchPad.ValueMode mode = getSkinnable().
            getValueMode();

    // labels
    if (mode != null) {
      gc.setFill(getSkinnable().
              getDataLabelsColor());
      switch (mode) {
        case ZERO_TO_ONE:
          gc.fillText("0", 3, size - 3);
          gc.fillText("y", 3, 14);
          gc.fillText("x", size - 10, size - 3);
          break;
        case NEGATIVE_TO_POSITIVE:
          gc.fillText("0", size / 2 + 3, size / 2 - 3);
          gc.fillText("X", size - 10, size / 2 - 3);
          gc.fillText("-x", 3, size / 2 - 3);
          gc.fillText("y", size / 2 + 5, 14);
          gc.fillText("-y", size / 2 + 5, size - 10);
          break;
      }
    }
  }

  private void drawCrosslines(double x, double y) {

    // keep within control size
    if (Double.compare(x, 0.0) <= 0) {
      x = 1.0;
    } else if (x > size) {
      x = size - 1.0;
    }

    if (Double.compare(y, 0.0) <= 0) {
      y = 1.0;
    } else if (y > size) {
      y = size - 1.0;
    }

    GraphicsContext gc = touchLayer.getGraphicsContext2D();
    clearTouchLayer();
    gc.setStroke(getSkinnable().
            getCrossLinesColor());
    gc.setLineWidth(1);
    gc.strokeLine(x, 0, x, size);
    gc.strokeLine(0, y, size, y);
  }

  private void clearTouchLayer() {
    GraphicsContext gc = touchLayer.getGraphicsContext2D();
    gc.clearRect(0, 0, size, size);
  }

  private void clearLabelLayer() {
    GraphicsContext gc = labelLayer.getGraphicsContext2D();
    gc.clearRect(0, 0, size, size);
  }
}