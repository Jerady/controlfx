/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.touchpad;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Control;
import javafx.scene.paint.Color;

/**
 *
 * @author Jens Deters
 */
public class TouchPad extends Control {

  public static final String DEFAULT_STYLE_CLASS = "touchpad";
  public static final String X_VALUE = "xValue";
  public static final String Y_VALUE = "yValue";
  public static final String VALUE_MODE = "valueMode";
  public static final String MOVE_BACK_ON_RELEASE = "moveBackOnRelease";
  public static final String ANIMATE_MOVE_BACK_ON_RELEASE = "animateMoveBackOnRelease";
  public static final String DATA_GRID_COLOR = "dataGridColor";
  public static final String DATA_LABELS_COLOR = "dataLabelsColor";
  public static final String CROSSLINES_COLOR = "crossLinesColor";
  public static final String ANIMATION_DURATION = "animationDuration";
  private DoubleProperty xValueProperty;
  private double defaultXValue = 0.0;
  private DoubleProperty yValueProperty;
  private double defaultYValue = 0.0;
  private ObjectProperty<ValueMode> valueModeProperty;
  private ValueMode defaultValueMode = ValueMode.ZERO_TO_ONE;
  private ObjectProperty<Color> dataGridColorProperty;
  private Color defaultDataGridColor = Color.WHITE;
  private ObjectProperty<Color> dataLabelsColorProperty;
  private Color defaultDataLabelsColor = Color.WHITE;
  private ObjectProperty<Color> crossLinesColorProperty;
  private Color defaultCrossLinesColor = Color.YELLOWGREEN;
  private BooleanProperty moveBackOnReleaseProperty;
  private boolean defaultMoveBackOnRelease = false;
  private BooleanProperty animateMoveBackOnReleaseProperty;
  private boolean defaultAnimateMoveBackOnRelease = false;
  private DoubleProperty animationDurationMillisProperty;
  private double defaultAnimationDuration = 350.0;

  public enum ValueMode {

    ZERO_TO_ONE, NEGATIVE_TO_POSITIVE;
  }

  public TouchPad() {
    init();
  }

  private void init() {
    moveBackOnReleaseProperty = new SimpleBooleanProperty(this, MOVE_BACK_ON_RELEASE, defaultMoveBackOnRelease);
    animateMoveBackOnReleaseProperty = new SimpleBooleanProperty(this, ANIMATE_MOVE_BACK_ON_RELEASE, defaultAnimateMoveBackOnRelease);
    animationDurationMillisProperty = new SimpleDoubleProperty(this, ANIMATION_DURATION, defaultAnimationDuration);
    xValueProperty = new SimpleDoubleProperty(this, X_VALUE, defaultXValue);
    yValueProperty = new SimpleDoubleProperty(this, Y_VALUE, defaultYValue);
    valueModeProperty = new SimpleObjectProperty<>(this, VALUE_MODE, defaultValueMode);
    dataGridColorProperty = new SimpleObjectProperty<>(this, DATA_GRID_COLOR, defaultDataGridColor);
    dataLabelsColorProperty = new SimpleObjectProperty<>(this, DATA_LABELS_COLOR, defaultDataLabelsColor);
    crossLinesColorProperty = new SimpleObjectProperty<>(this, CROSSLINES_COLOR, defaultCrossLinesColor);
    xValueProperty = new SimpleDoubleProperty(this, X_VALUE, defaultXValue);

    getStyleClass().
            add(DEFAULT_STYLE_CLASS);

  }

  @Override
  public void setPrefSize(final double WIDTH, final double HEIGHT) {
    final double SIZE = WIDTH <= HEIGHT ? WIDTH : HEIGHT;
    super.setPrefSize(SIZE, SIZE);
  }

  @Override
  public void setMinSize(final double WIDTH, final double HEIGHT) {
    final double SIZE = WIDTH <= HEIGHT ? WIDTH : HEIGHT;
    super.setMinSize(SIZE, SIZE);
  }

  @Override
  public void setMaxSize(final double WIDTH, final double HEIGHT) {
    final double SIZE = WIDTH <= HEIGHT ? WIDTH : HEIGHT;
    super.setMaxSize(SIZE, SIZE);
  }

  /*
   * -------------- PROPERTIES --------------
   */
  public ReadOnlyDoubleProperty xValueReadOnlyProperty() {
    return xValueProperty;
  }

  public ReadOnlyDoubleProperty yValueReadOnlyProperty() {
    return yValueProperty;
  }

  public void setXValue(double xValue) {
    xValueProperty.set(xValue);
  }

  public double getXValue() {
    return xValueProperty.get();
  }

  public void setYValue(double yValue) {
    yValueProperty.set(yValue);
  }

  public double getYValue() {
    return yValueProperty.get();
  }

  public double getAnimationDurationMillis() {
    return animationDurationMillisProperty.get();
  }

  public Color getDataLabelsColor() {
    return dataLabelsColorProperty.get();
  }

  public Color getDataGridColor() {
    return dataGridColorProperty.get();
  }

  public Color getCrossLinesColor() {
    return crossLinesColorProperty.get();
  }

  public ValueMode getValueMode() {
    return valueModeProperty.get();
  }

  public Boolean isAnimateMoveBackOnRelease() {
    return animateMoveBackOnReleaseProperty.get();
  }

  public Boolean isMoveBackOnRelease() {
    return moveBackOnReleaseProperty.get();
  }

  public void setDataGridColor(Color dataGridColor) {
    this.dataGridColorProperty.set(dataGridColor);
  }

  public void setDataLabelsColor(Color dataLabelsColor) {
    this.dataGridColorProperty.set(dataLabelsColor);
  }

  public void setCrossLinesColor(Color crossLinesColor) {
    this.crossLinesColorProperty.set(crossLinesColor);
  }

  public void setValueMode(ValueMode valueModeProperty) {
    this.valueModeProperty.set(valueModeProperty);
  }

  public DoubleProperty animationDurationMillisProperty() {
    return animationDurationMillisProperty;
  }

  public ObjectProperty<ValueMode> valueModeProperty() {
    return valueModeProperty;
  }

  public BooleanProperty animateMoveBackOnReleaseProperty() {
    return animateMoveBackOnReleaseProperty;
  }

  public BooleanProperty moveBackOnReleaseProperty() {
    return moveBackOnReleaseProperty;
  }

  public DoubleProperty xValueProperty() {
    return xValueProperty;
  }

  public DoubleProperty yValueProperty() {
    return yValueProperty;
  }

  public ObjectProperty<Color> dataGridColorProperty() {
    return dataGridColorProperty;
  }

  public ObjectProperty<Color> dataLabelsColorProperty() {
    return dataLabelsColorProperty;
  }

  public ObjectProperty<Color> crossLinesColorProperty() {
    return crossLinesColorProperty;
  }

  /*
   * -------------- Stylesheet handling --------------
   */
  @Override
  public String getUserAgentStylesheet() {
    return getClass().
            getResource("touchpad.css").
            toExternalForm();
  }
}
