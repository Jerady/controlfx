/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.touchpad.demo;

import de.jensd.fx.control.touchpad.TouchPad;
import de.jensd.fx.control.touchpad.TouchPadBuilder;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Jens Deters
 */
public class TouchPadPane extends AnchorPane implements Initializable {

  @FXML
  private ChoiceBox<TouchPad.ValueMode> valueModeChoiceBox;
  @FXML
  private Label xValueLabel;
  @FXML
  private Label xValuePercentLabel;
  @FXML
  private Label yValueLabel;
  @FXML
  private Label yValuePercentLabel;
  @FXML
  private CheckBox moveBackOnReleaseCheckBox;
  @FXML
  private CheckBox animateMoveBackOnReleaseCheckBox;
  @FXML
  private Label animationDurationMillisLabel;
  @FXML
  private Slider animationDurationMillisSlider;
  @FXML
  private Button closeButton;
  private TouchPad touchPad;

  public TouchPadPane() {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().
            getResource(
            "TouchPadPane.fxml"));
    fxmlLoader.setRoot(this);
    fxmlLoader.setController(this);
    try {
      fxmlLoader.load();
    } catch (IOException exception) {
      throw new RuntimeException(exception);
    }
  }

  /**
   * Initializes the controller class.
   */
  @Override
  public void initialize(URL url, ResourceBundle rb) {

    assert animateMoveBackOnReleaseCheckBox != null : "fx:id=\"animateMoveBackOnReleaseCheckBox\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert animationDurationMillisSlider != null : "fx:id=\"animationDurationMillisSlider\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert animationDurationMillisLabel != null : "fx:id=\"animationDurationMillisLabel\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert moveBackOnReleaseCheckBox != null : "fx:id=\"moveBackOnReleaseCheckBox\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert valueModeChoiceBox != null : "fx:id=\"valueModeChoiceBox\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert xValueLabel != null : "fx:id=\"xValueLabel\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert xValuePercentLabel != null : "fx:id=\"xValuePercentLabel\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert yValueLabel != null : "fx:id=\"yValueLabel\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert yValuePercentLabel != null : "fx:id=\"yValuePercentLabel\" was not injected: check your FXML file 'TouchPadPane.fxml'.";
    assert closeButton != null : "fx:id=\"closeButton\" was not injected: check your FXML file 'TouchPadPane.fxml'.";

    touchPad = TouchPadBuilder.create().
            prefHeight(300).
            prefWidth(300).
            build();

    xValuePercentLabel.textProperty().
            bind(touchPad.xValueReadOnlyProperty().
            multiply(100.0).
            asString("%.2f %%"));
    yValuePercentLabel.textProperty().
            bind(touchPad.yValueReadOnlyProperty().
            multiply(100.0).
            asString("%.2f %%"));

    xValueLabel.textProperty().
            bind(touchPad.xValueReadOnlyProperty().
            asString("%.2f"));
    yValueLabel.textProperty().
            bind(touchPad.yValueReadOnlyProperty().
            asString("%.2f"));

    moveBackOnReleaseCheckBox.selectedProperty().
            bindBidirectional(touchPad.moveBackOnReleaseProperty());
    animateMoveBackOnReleaseCheckBox.selectedProperty().
            bindBidirectional(touchPad.animateMoveBackOnReleaseProperty());

    animateMoveBackOnReleaseCheckBox.disableProperty().
            bind(moveBackOnReleaseCheckBox.selectedProperty().
            not());
    animationDurationMillisSlider.disableProperty().
            bind(animateMoveBackOnReleaseCheckBox.selectedProperty().
            not());
    animationDurationMillisLabel.disableProperty().
            bind(animateMoveBackOnReleaseCheckBox.selectedProperty().
            not());
    animationDurationMillisLabel.textProperty().
            bind(animationDurationMillisSlider.valueProperty().
            asString("Animation Duration: %.0f Millis"));
    animationDurationMillisSlider.valueProperty().
            bindBidirectional(touchPad.animationDurationMillisProperty());

    closeButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent t) {
        Platform.exit();
      }
    });

    ObservableList< TouchPad.ValueMode> modes = FXCollections.
            observableArrayList();
    modes.add(TouchPad.ValueMode.ZERO_TO_ONE);
    modes.add(TouchPad.ValueMode.NEGATIVE_TO_POSITIVE);

    AnchorPane.setTopAnchor(touchPad, 50.0);
    AnchorPane.setLeftAnchor(touchPad, 90.0);

    valueModeChoiceBox.setItems(modes);
    valueModeChoiceBox.valueProperty().
            bindBidirectional(touchPad.valueModeProperty());

    touchPad.setXValue(0.0);
    touchPad.setYValue(0.0);

    setPrefSize(700, 500);

    getChildren().
            add(touchPad);
  }
}
