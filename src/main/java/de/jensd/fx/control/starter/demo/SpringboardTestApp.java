/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.starter.demo;

import de.jensd.fx.control.starter.Starter;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters
 */
public class SpringboardTestApp extends Application
{

    @Override
    public void start(Stage primaryStage)
    {

        ObservableList<Starter> springs = FXCollections
                .observableArrayList();

        String hoverBaseImage = "/de/jensd/fx/control/starter/demo/cloud_download.png";


        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));

        AnchorPane springPane = new AnchorPane();
        springPane.getChildren()
                .addAll(springs);


        double angle = 0;
        double angleStep = 360 / springs.size();
        double centerX = 200.0;
        double centerY = 200.0;

        for (Starter hoverSpringBoard : springs)
        {
            hoverSpringBoard.setLayoutX(150.0 * Math.cos(Math
                    .toRadians(angle)) + centerX);
            hoverSpringBoard.setLayoutY(150.0 * Math.sin(Math
                    .toRadians(angle)) + centerY);

            angle += angleStep;
        }

        StackPane root = new StackPane();
        root.getChildren()
                .addAll(springPane);

        Scene scene = new Scene(root, 500, 500);

        primaryStage.setTitle("SpringBoardDemo");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
