/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.control.starter;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.event.EventHandler;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.DropShadowBuilder;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 *
 * @author Jens Deters
 */
public class Starter extends StackPane {

  private ParallelTransition hoverTransition;
  private ParallelTransition startTransition;
  private ScaleTransition scaleTransition;
  private ScaleTransition initTransition;
  private FadeTransition fadeTransition;
  private SequentialTransition startsSequentialTransition;
  private double toMax;
  private ImageView baseImage;

  public Starter(String base, double fitWidth, double fitHeight) {
    baseImage = new ImageView(base);
    baseImage.setFitWidth(fitWidth);
    baseImage.setFitHeight(fitHeight);
    init();
  }

  private void init() {

    Color selectedColor = Color.rgb(0, 0, 0, 0.5);
    DropShadow dropShadow = DropShadowBuilder.create().
            color(selectedColor).
            build();
    setEffect(dropShadow);

    dropShadow.radiusProperty().
            bind(scaleXProperty().
            multiply(3));
    dropShadow.offsetXProperty().
            bind(scaleXProperty().
            multiply(3));
    dropShadow.offsetYProperty().
            bind(scaleXProperty().
            multiply(3));

    toMax = 1.2;

    scaleTransition =
            new ScaleTransition(Duration.millis(200), this);
    scaleTransition.setCycleCount(1);
    scaleTransition
            .setInterpolator(Interpolator.EASE_BOTH);

    initTransition =
            new ScaleTransition(Duration.millis(200), this);
    initTransition.setToX(1);
    initTransition.setToY(1);
    initTransition.setCycleCount(1);
    initTransition
            .setInterpolator(Interpolator.EASE_BOTH);

    hoverTransition = new ParallelTransition();
    hoverTransition.getChildren().
            addAll(
            scaleTransition);

    fadeTransition = new FadeTransition(Duration.millis(200), this);
    fadeTransition.setCycleCount(1);
    fadeTransition
            .setInterpolator(Interpolator.EASE_BOTH);

    startTransition = new ParallelTransition();
    startTransition.setCycleCount(2);
    startTransition.setAutoReverse(true);
    startTransition.getChildren().
            addAll(scaleTransition,
            fadeTransition);

    startsSequentialTransition = new SequentialTransition();
    startsSequentialTransition.getChildren().
            addAll(
            startTransition,
            initTransition);
    getChildren().
            addAll(baseImage);


    setOnMouseEntered(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent t) {
        scaleTransition.setFromX(getScaleX());
        scaleTransition.setFromY(getScaleY());
        scaleTransition.setToX(toMax);
        scaleTransition.setToY(toMax);
        hoverTransition.playFromStart();
      }
    });

    setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent t) {
        scaleTransition.setFromX(getScaleX());
        scaleTransition.setFromY(getScaleY());
        scaleTransition.setToX(1);
        scaleTransition.setToY(1);
        hoverTransition.playFromStart();
      }
    });

    setOnMouseClicked(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent t) {
        scaleTransition.setFromX(getScaleX());
        scaleTransition.setFromY(getScaleY());
        scaleTransition.setToX(2);
        scaleTransition.setToY(2);
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.5f);
        startsSequentialTransition.playFromStart();
      }
    });

  }
}