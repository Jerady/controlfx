/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.fx.styles;

/**
 *
 * @author Jens
 */
public enum ThemeStyle {

  LIGHT("/de/jensd/fx/themes/light.css"),
  DARK("/de/jensd/fx/themes/dark.css"),
  CASPIAN("com/sun/javafx/scene/control/skin/caspian/caspian.css");
  private final String styleName;

  private ThemeStyle(String stylePath) {
    this.styleName = stylePath;
  }

  public String styleName() {
    return styleName;
  }

  @Override
  public String toString() {
    return styleName;
  }
}
