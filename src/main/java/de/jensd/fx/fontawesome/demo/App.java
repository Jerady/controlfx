/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.fontawesome.demo;

import de.jensd.fx.dudes.AwesomeDude;
import de.jensd.fx.dudes.StyleDude;
import de.jensd.fx.fontawesome.AwesomeIcons;
import de.jensd.fx.fontawesome.AwesomeIconsStyle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class App extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {

    AnchorPane root = new AnchorPane();
    Scene scene = new Scene(root, 500, 500);
    scene.getStylesheets().
            addAll(AwesomeIconsStyle.ICONS_STYLE_PLAIN.styleName());

    Font.loadFont(App.class
            .getResource("/META-INF/resources/webjars/font-awesome/3.0.0/font/fontawesome-webfont.ttf").
            toExternalForm(), 12);

    ObservableList<Label> icons1 = FXCollections
            .observableArrayList();

    icons1.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_ENVELOPE, 32));
    icons1.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_TAGS, 32));
    icons1.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_TASKS, 32));

    Label up = AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_THUMBS_UP, 32);
    up.setDisable(true);
    Label down = AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_THUMBS_DOWN, 32);
    down.setDisable(true);

    icons1.add(up);
    icons1.add(down);

    ObservableList<Label> icons2 = FXCollections
            .observableArrayList();

    icons2.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_PICTURE, 32));

    Label tags = AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_TAGS, 32);
    tags.setDisable(true);
    Label tasks = AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_TASKS, 32);
    tasks.setDisable(true);

    icons2.add(tags);
    icons2.add(tasks);
    icons2.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_THUMBS_UP, 32));
    icons2.add(AwesomeDude
            .createIconLabel(AwesomeIcons.ICON_THUMBS_DOWN, 32));


    Button button1 = AwesomeDude
            .createIconButton(AwesomeIcons.ICON_REFRESH, "Button Dummy");
    Button button2 = AwesomeDude
            .createIconButton(AwesomeIcons.ICON_REFRESH, 256);
    button2.setTranslateY(150.0);
    button2.setTranslateX(100.0);

    MenuButton styleChooser = new MenuButton("Choose Style");
    styleChooser
            .setEffect(new DropShadow(BlurType.GAUSSIAN, Color.YELLOWGREEN, 5, 1.0, 0, 0));
    MenuButton themeChooser = StyleDude.createThemeMenu("Choose Theme", scene);
    themeChooser
            .setEffect(new DropShadow(BlurType.GAUSSIAN, Color.YELLOWGREEN, 5, 1.0, 0, 0));

    ToolBar toolBarHorizontal = new ToolBar();
    toolBarHorizontal.getItems().
            addAll(icons1);
    toolBarHorizontal.getItems().
            addAll(button1);
    toolBarHorizontal.getItems().
            addAll(themeChooser);
    toolBarHorizontal.getItems().
            addAll(styleChooser);
    AnchorPane.setTopAnchor(toolBarHorizontal, Double.MIN_VALUE);
    AnchorPane.setRightAnchor(toolBarHorizontal, Double.MIN_VALUE);
    AnchorPane.setLeftAnchor(toolBarHorizontal, Double.MIN_VALUE);

    ToolBar toolBarVertical = new ToolBar();
    toolBarVertical.setOrientation(Orientation.VERTICAL);
    toolBarVertical.getItems().
            addAll(icons2);
    toolBarVertical.setTranslateY(80.0);


    root.getChildren().
            addAll(button2, toolBarHorizontal, toolBarVertical);





    MenuItem stylePlain = StyleDude
            .createMenuItem("Plain", scene, AwesomeIconsStyle.ICONS_STYLE_PLAIN.
            styleName());
    MenuItem styleDark = StyleDude
            .createMenuItem("Dark", scene, AwesomeIconsStyle.ICONS_STYLE_DARK.
            styleName());
    MenuItem styleLight = StyleDude
            .createMenuItem("Light", scene, AwesomeIconsStyle.ICONS_STYLE_LIGHT.
            styleName());
    MenuItem styleBlue = StyleDude
            .createMenuItem("Blue", scene, AwesomeIconsStyle.ICONS_STYLE_BLUE.
            styleName());
    MenuItem styleRed = StyleDude
            .createMenuItem("Red", scene, AwesomeIconsStyle.ICONS_STYLE_RED.
            styleName());
    styleChooser.getItems().
            clear();
    styleChooser.getItems().
            addAll(stylePlain, styleLight, styleDark, styleBlue, styleRed);

    primaryStage.setScene(scene);
    primaryStage.setTitle("FontAwesomeFX DEMO");
    primaryStage.show();

  }

  public static void main(String[] args) {
    launch(args);
  }
}
