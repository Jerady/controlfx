/**
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the <organization> nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL JENS DETERS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.jensd.fx.dudes;

import de.jensd.fx.styles.ThemeStyle;
import de.jensd.fx.fontawesome.AwesomeIconsStyle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Jens Deters
 * @since 15.02.2013
 * @version 1.0
 */
public class StyleDude {

  private static final Logger LOGGER = Logger.
          getLogger(StyleDude.class.getName());

  public static MenuItem createMenuItem(String label, final Scene scene, String... cssFileNames) {
    MenuItem menuItem = new MenuItem(label);
    ObservableList<String> cssStyles = FXCollections.observableArrayList();
    for (String cssFileName : cssFileNames) {
      cssStyles.addAll(loadSkin(cssFileName));
    }
    menuItem.setOnAction(createSkinFormAction(cssStyles, scene));
    return menuItem;
  }

  public static ObservableList<String> loadSkin(String cssFileName) {
    LOGGER.log(Level.INFO, "Attempt to load css: {0}", cssFileName);
    ObservableList<String> cssStyles = FXCollections.observableArrayList();

    try {
      cssStyles.addAll(StyleDude.class.getResource(cssFileName).
              toExternalForm());

    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, "Error while loading: {0}", cssFileName);
    }

    return cssStyles;
  }

  public static EventHandler<ActionEvent> createSkinFormAction(final ObservableList<String> cssStyle, final Scene scene) {
    return new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent t) {
        scene.getStylesheets().
                addAll(cssStyle);
      }
    };
  }

  public static MenuButton createThemeMenu(String name, Scene scene) {
    MenuButton styleChooser = new MenuButton(name);

    MenuItem styleDark = StyleDude
            .createMenuItem("Dark", scene, ThemeStyle.DARK.
            styleName());
    MenuItem styleLight = StyleDude
            .createMenuItem("Light", scene, ThemeStyle.LIGHT.
            styleName());
    MenuItem styleCaspian = StyleDude
            .createMenuItem("Caspian", scene, ThemeStyle.CASPIAN.
            styleName());
    styleChooser.getItems().
            addAll(styleDark, styleLight, styleCaspian);

    return styleChooser;
  }
}
